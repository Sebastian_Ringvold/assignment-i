#pragma once

template<typename T>
struct Node
{
	T m_data;
	Node *m_next;
	Node<T>* m_left;
	Node<T>* m_right;

	Node()
	{
		m_left = nullptr;
		m_next = nullptr;
		m_right = nullptr;
	}
};
