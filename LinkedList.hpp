//LinkedList.hpp
#include "Node.hpp"

template <typename T>
class LinkedList
{
	Node<T> *m_root;
	int m_size;

public:
	LinkedList();

	void push_front(T n);
	void push_back(T n);
	void pop_front();
	void pop_back();
	int size();
	bool find(T n);
	void clear();
};

template <typename T>
LinkedList<T>::LinkedList()
{
	m_root = nullptr;
}

template <typename T>
int LinkedList<T>::size()
{
	return m_size;
}

template <typename T>
void LinkedList<T>::push_front(T n)
{
	Node<T> *newNode = new Node<T>;
	newNode->m_data = n;
	newNode->m_next = m_root;
	m_root = newNode;
	m_size++;
}

template <typename T>
void LinkedList<T>::push_back(T n)
{
	Node<T> *tmp = m_root;
	Node<T>* newNode = new Node <T>;
	newNode->m_data = n;

	if (tmp == nullptr)
		m_root = newNode;
	else
	{
		while (tmp->m_next != nullptr)
		{
			tmp = tmp->m_next;
		}
		tmp->m_next = newNode;
	}
	m_size++;
}

template <typename T>
void LinkedList<T>::pop_front()
{
	Node<T>* tmp = m_root->m_next;
	delete m_root;
	m_root = tmp;
	m_size--;
}

template <typename T>
void LinkedList<T>::pop_back()
{
	if (m_size == 0) return;
	if (m_size-- == 1)
	{
		delete m_root;
		m_root = nullptr;
		return;
	}
	Node<T>* tmp = m_root;
	Node<T>* tmp2 = nullptr;

	while (tmp->m_next != nullptr)
	{
		tmp2 = tmp;
		tmp = tmp->m_next;
	}
	delete tmp;
	tmp2->m_next = nullptr;
}

template <typename T>
void LinkedList<T>::clear()
{
	while (m_size != 0)
	{
		pop_back();
	}
}

template <typename T>
bool LinkedList<T>::find(T n) 
{
	Node<T>* tmp = m_root;
	while (tmp != nullptr)
	{
		if (tmp->m_data == n)
		{
			return true;
		}
		tmp = tmp->m_next;
	}
	return false;
}



