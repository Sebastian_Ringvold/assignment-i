#pragma once
#include <iostream>
#include "Node.hpp"

template<typename T>
class BinarySearchTree
{
private:
	Node<T>* m_root;
public:
	BinarySearchTree();
	void insertNode(T n);
	bool searchTree(Node<T>* node, const T& n);
	bool search(const T& n);
	void createNode(T n);
	
	void TraversalInOrder(Node<T>* n) const;
	void TraversalPreOrder(Node<T>* n) const;
	void TraversalPostOrder(Node<T>* n) const;
	void clearTree(Node<T>* node);
	void clear();

	bool traversal_in_order();
	bool traversal_pre_order();
	bool traversal_post_order();
	void size(Node<T>* node, int &n);
	int getSize();
};

template<typename T>
BinarySearchTree<T>::BinarySearchTree()
{
	m_root = nullptr;
}

template<typename T>
void BinarySearchTree<T>::createNode(T n)
{
	Node<T>* node = new Node<T>();
	node->m_data = n;
	return node;
}

template<typename T>
void BinarySearchTree<T>::insertNode(T n)
{
	Node<T>* node = m_root;
	
	if (m_root == nullptr)
	{
		m_root = new Node<T>();
		m_root->m_data = n;
		return;
	}

	while (node != nullptr)
	{
		if (n == node->m_data)
			return;
		else if (n > node->m_data)
		{
			if (node->m_right == nullptr)
			{
				node->m_right = new Node <T>;
				node->m_right->m_data = n;
			}
			else
			{
				node = node->m_right;
			}
		}
		else
		{
			if (node->m_left == nullptr)
			{
				node->m_left = new Node <T>;
				node->m_left->m_data = n;
			}
			else
			{
				node = node->m_left;
			}
		}
	}
}

template<typename T>
bool BinarySearchTree<T>::searchTree(Node<T>* node, const T& n)
{
	if (node == nullptr)
		return false;
	if (n == node->m_data)
		return true;

	else if (n < node->m_data)
	{
		return searchTree(node->m_left, n);
	}

	else
	{
		return searchTree(node->m_right, n);
	}
}

template<typename T>
bool BinarySearchTree<T>::search(const T& n)
{
	return searchTree(m_root, n);
}

template<typename T>
void BinarySearchTree<T>::TraversalInOrder(Node<T>* n) const
{
	if (n != nullptr)
	{
		if (n->m_left != nullptr)
			TraversalInOrder(n->m_left);

		std::cout << n->m_data << " ";

		if (n->m_right != nullptr)
			TraversalInOrder(n->m_right);
	}
}

template<typename T>
void BinarySearchTree<T>::TraversalPreOrder(Node<T>* n) const
{
	if (n != nullptr)
	{
		std::cout << n->m_data << " ";

		if (n->m_left != nullptr)
			TraversalPreOrder(n->m_left);
		if (n->m_right != nullptr)
			TraversalPreOrder(n->m_right);
	}
}
template<typename T>
void BinarySearchTree<T>::TraversalPostOrder(Node<T>* n) const
{
	if (n != nullptr)
	{

		if (n->m_left != nullptr)
			TraversalPostOrder(n->m_left);
		if (n->m_right != nullptr)
			TraversalPostOrder(n->m_right);
		std::cout << n->m_data << " ";
	}
}

template<typename T>
bool BinarySearchTree<T>::traversal_in_order()
{
	if (m_root)
	{
		TraversalInOrder(m_root);
		return true;
	}
	return false;
}

template<typename T>
bool BinarySearchTree<T>::traversal_pre_order()
{
	if (m_root)
	{
		TraversalPreOrder(m_root);
		return true;
	}
	return false;
}

template<typename T>
bool BinarySearchTree<T>::traversal_post_order()
{
	if (m_root)
	{
		TraversalPostOrder(m_root);
		return true;
	}
	return false;
}

template<typename T>
void BinarySearchTree<T>::clearTree(Node<T>* node)
{
	if (node != nullptr)
	{
		clearTree(node->m_left);
		clearTree(node->m_right);
		node = nullptr;
			delete node;
	}
	if (node == nullptr)
		return;
}

template<typename T>
void BinarySearchTree<T>::clear()
{
	clearTree(m_root);
	m_root = nullptr;
}

template<typename T>
void BinarySearchTree<T>::size(Node<T>* node, int &n)
{
	if (node == NULL)
		return;
	++n;
	size(node->m_left, n); 
	size(node->m_right, n);
}

template<typename T>
int BinarySearchTree<T>::getSize()
{
	int n = 0;
	size(m_root, n);
	std::cout << " Size of tree: " << n << std::endl;
	return n;
}