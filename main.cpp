// main.cpp
#include "LinkedList.hpp"
#include "BinarySearchTree.hpp"
#include "Unit.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
	BinarySearchTree<int> BinarySearchTree;
	LinkedList<int> LinkedList;

	std::cout << "  " << std::endl;
	std::cout << "  " << std::endl;//mellanrum f�r l�slighet i konsol
	std::cout << "///////////////////Binary Search Tree Test///////////////////" << std::endl;

	//Binary Search Tree tests

	//Inserting Nodes 
	BinarySearchTree.insertNode(1);
	BinarySearchTree.insertNode(3);
	BinarySearchTree.insertNode(5);
	BinarySearchTree.insertNode(2);
	BinarySearchTree.insertNode(7);
	
	//Searching Nodes
	verify(BinarySearchTree.search(3), true, "Test Insert and Search once");
	verify(BinarySearchTree.search(5), true, "twice");
	verify(BinarySearchTree.search(1), true, "..and thrice.");
	std::cout << "  " << std::endl;

	//traversal
	std::cout << "Testing traversal in order:" << std::endl;
	BinarySearchTree.traversal_in_order();
	std::cout << "  " << std::endl;
	std::cout << "Testing traversal pre order:" << std::endl;
	BinarySearchTree.traversal_pre_order();
	std::cout << "  " << std::endl;
	std::cout << "Testing traversal post order:" << std::endl;
	BinarySearchTree.traversal_post_order();

	//size
	std::cout << "  " << std::endl;
	verify(BinarySearchTree.getSize(), 5, "Testing Size");
	
	//clear
	BinarySearchTree.clear();
	verify(BinarySearchTree.getSize(), 0, "Size Test after clear function");



	//Linked List Tests

	//push front and back
	LinkedList.push_front(7);
	LinkedList.push_front(5);
	LinkedList.push_front(11);
	LinkedList.push_back(2);
	LinkedList.push_back(9);

	std::cout << "  " << std::endl;
	std::cout << "  " << std::endl;
	std::cout << "  " << std::endl;//mellanrum f�r l�slighet i konsol
	std::cout << "///////////////////Linked list test///////////////////" << std::endl;
	
	std::cout << "Testing the find function: " << std::endl;
	verify(true, LinkedList.find(7), "find test 1");
	verify(true, LinkedList.find(2), "find test 2");

	//size test
	verify(5, LinkedList.size(), "size test before pop back and front");

	//pop back and front
	LinkedList.pop_back();
	LinkedList.pop_front();

	verify(3, LinkedList.size(), "size test after pop back and front");
    
	LinkedList.clear();

	verify(0, LinkedList.size(), "size test after clear");




	std::cin.get();
	return 0;
}